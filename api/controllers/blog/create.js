module.exports = {
  friendlyName: "Create",

  description: "Create blog.",

  inputs: {
    title: {
      type: "string",
      required: true,
      description: "Full representation of the Blog title",
      maxLength: 255,
      example: "USA Elections",
    },
    content: {
      type: "string",
      required: true,
      description: "The Content of the blog",
    },
  },

  exits: {},

  fn: async function ({ title, content }) {
    // All done.
    var newBlog = await Blog.create({
      title: title,
      content: content,
      slug: _.snakeCase(title),
    }).fetch();
    return newBlog;
  },
};
