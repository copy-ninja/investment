module.exports = {
  friendlyName: "Find all",

  description: "",

  inputs: {},

  exits: {
    success: {
      viewTemplatePath: "pages/blog/index",
      description: "Display all the blogs in the database",
    },
  },

  fn: async function (inputs) {
    var allBlogs = await Blog.find();
    // All done.
    return {allBlogs};
  },
};
