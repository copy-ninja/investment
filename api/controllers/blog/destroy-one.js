module.exports = {
  friendlyName: "Destroy one",

  description: "",

  inputs: {
    title: {
      type: "string",
      required:true,
      description: "Full representation of the url slug",
      example: "usa_elections",
    },
  },

  exits: {},

  fn: async function ({ title }) {
    // All done.
    var removedBlog = await Blog.destroyOne({
      slug: title,
    });
    return removedBlog;
  },
};
