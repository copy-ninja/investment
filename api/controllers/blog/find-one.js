module.exports = {
  friendlyName: "Find one",

  description: "",

  inputs: {
    title: {
      type: "string",
      description: "Full representation of the url slug",
      maxLength: 255,
      example: "usa_elections",
    },
  },

  exits: {
    success: {
      viewTemplatePath: "pages/blog/post",
      description: "Display a blog post",
    },
  },

  fn: async function ({ title }) {
    // All done.
    var blogPost = await Blog.findOne({ slug: title });
    return { blogPost };
  },
};
